package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Button;

public class StartActivity extends AppCompatActivity {
    RelativeLayout relativeLayout;
    private ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        imageButton = (ImageButton) findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openActivityImage();
            }
        });
    }
    public void openActivityImage() {
        Intent intent = new Intent(this, ImageActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){

    }

}
