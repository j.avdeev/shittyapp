package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ThreadLocalRandom;

public class ImageActivity extends AppCompatActivity {
    ImageView imageView;
    int fromValue = 2; //included
    int toValue = 3;   //non-included
    int randomNumber = generateRandomNumberBetweenRange(fromValue, toValue);

    String url = "https://gitlab.com/j-avdeev/shittyapp/-/raw/master/app/src/main/res/drawable/" + randomNumber + ".png";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
               
        imageView = findViewById(R.id.imageView);
        loadImageFromUrl(url);
        
            Context context = getApplicationContext();        
        Picasso.get()
                .load( url )
//                .error( R.drawable.ic_error )
                .error( R.drawable.custom_button )
//                .placeholder( R.drawable.progress_animation )
                .into( imageView );
        

//            CharSequence text = "Hello toast!";
//            int duration = Toast.LENGTH_SHORT;
//
//            Toast toast = Toast.makeText(context, text, duration);
//            toast.show();


    }
    private void loadImageFromUrl(String url) {
        Picasso.get().load(url).into(imageView);
    }

    private int generateRandomNumberBetweenRange(int from, int to){
        return ThreadLocalRandom.current().nextInt(fromValue, toValue);
    }

    @Override
    public void onBackPressed(){

    }

}
